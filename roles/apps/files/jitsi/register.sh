# THIS FILE IS MANAGED BY ANSIBLE
#!/usr/bin/env bash

cd "$(dirname "$0")"
chown -R 101:root data/prosody/config/

USERNAME=$1
read -rsp "password: " PASSWORD; echo

if [[ -f "data/prosody/config/data/meet%2ejitsi/accounts/$USERNAME.dat" ]]; then
    echo "User $USERNAME exists"
    exit 1
fi

docker compose exec prosody \
    /usr/bin/prosodyctl --config /config/prosody.cfg.lua \
    register $USERNAME meet.jitsi $PASSWORD
