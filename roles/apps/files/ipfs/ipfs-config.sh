# THIS FILE IS MANAGED BY ANSIBLE
#!/bin/sh

set -ex

ipfs config --json API.HTTPHeaders.Access-Control-Allow-Origin '["https://'$LOCAL_DOMAIN'"]'
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Methods '["PUT", "POST"]'

ipfs config --json Gateway.PublicGateways '{
        "'$IPFS_DOMAIN'": {
            "UseSubdomains": true,
            "Paths": ["/ipfs", "/ipns"]
        }
    }'

ipfs config --json DNS.Resolvers '{
        ".": "https://anycast.censurfridns.dk/dns-query"
    }'

ipfs config --json Datastore.StorageMax '"100GB"'
ipfs config --json Datastore.GCPeriod '"10m"'
