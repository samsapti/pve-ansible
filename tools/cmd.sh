#!/usr/bin/env bash

if [[ $1 == "-i" ]]; then
    ARGS="-i $2"
    shift 2
fi

HOSTS="$1"
CMD="$2"

eval "ansible $ARGS -b -m shell -a \"$CMD\" \"$HOSTS\""
